# !/usr/bin/python3

import threading
import socket
import time
import json
import random
import serial
import signal

# 定义服务器端口和IP地址
host = '127.0.0.1'
port = 33669
com = "COM3"
baudrate = 115200

json_data = {
    "state_vol": 1.00,  # 电压
    "state_en": 1   # 高低电平
}

signal_flag = False

def searial_reader():

    ser = serial.Serial(com, baudrate=baudrate)
    tmp2byte = [0,0]
    tmpdata = []
    readflag = False

    while not signal_flag:
        tmp2byte[0] = tmp2byte[1]
        idata = int.from_bytes(ser.read(),byteorder="big")
        tmp2byte[1] = idata

        if tmp2byte[0] == 0x55 and tmp2byte[1] == 0xaa and readflag == False:
            # head
            readflag = True
            continue

        elif tmp2byte[0] == 0xaa and tmp2byte[1] == 0x55 and readflag == True:
            # tail
            readflag = False
            tmpdata.pop(-1)
            print(tmpdata)

            # 校验和 (0x55 + 0xaa + 0xaa + 0x55 = 0xfe)
            if (sum(tmpdata) - tmpdata[-1] + 0xfe ) & 0xff != tmpdata[-1] & 0xff:
                tmpdata.clear()
                continue 
            
            # 功能码
            if tmpdata[0] == 2:
                json_data['state_vol'] = tmpdata[1] + tmpdata[2] / 100.0
            elif tmpdata[0] == 1:
                json_data['state_en'] = tmpdata[1]
            else:
                print("error func code")

            # print(json_data)
            tmpdata.clear()
            continue

        if readflag:
            tmpdata.append(idata)
      

def tfunc():
    while not signal_flag:
        json_data["state_en"] = random.randint(0,1)
        json_data["state_vol"] = random.randint(0,1000) / 100.0
        time.sleep(1)

def tcp_server():
    # 创建TCP服务器套接字
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # 绑定服务器地址
    server_socket.bind((host, port))

    # 监听连接
    server_socket.listen(1)
    print("服务器启动，等待连接...")

    # 等待客户端连接
    client_socket, client_address = server_socket.accept()
    print(f"有新的连接来自 {client_address}")

    # 每秒发送一次消息
    while not signal_flag:
        try:
            message = json.dumps(json_data)
            client_socket.sendall(message.encode())
            print(f"向客户端{client_address}发送消息: {message}")
            time.sleep(0.5)
        except Exception as e:
            print(e)
            break


if __name__ == "__main__":
    print("main.")
    
    th_searial_reader = threading.Thread(target=searial_reader)
    th_tcp_server = threading.Thread(target=tcp_server)
    th_searial_reader.start()
    th_tcp_server.start()

    while True:
        try:
            pass
        except KeyboardInterrupt:
            signal_flag = True
            time.sleep(1)
            break

    th_tcp_server.join()
    th_searial_reader.join()
