
// arduino esp32 file
// 用于模拟发送数据到串口

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  digitalWrite(LED_BUILTIN, HIGH);  // turn the LED on (HIGH is the voltage level)
  delay(1000);                      // wait for a second
  // Serial.println("hello world");
  char arr1[8] = {0x55,0xaa,0x02,0x02,0x03,0x06,0xaa,0x55};
  char arr2[7] = {0x55,0xaa,0x01,0x01,0x03,0xaa,0x55};
  arr1[3] = random(5);
  arr1[4] = random(100);
  arr2[3] = random(100) % 2;
  arr1[5] = (arr1[2] + arr1[3] + arr1[4] + 0xfe) & 0xff;
  arr2[4] = (arr2[2] + arr2[3] + 0xfe) & 0xff;
  for(int i = 0;i< 8;i++){
    Serial.write(arr1[i]);
  }
  delay(10);
  for(int i = 0;i< 7;i++){
    Serial.write(arr2[i]);
  }
  
  digitalWrite(LED_BUILTIN, LOW);   // turn the LED off by making the voltage LOW
  delay(1000);                      // wait for a second
}
