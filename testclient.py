# !/usr/bin/python3
import socket

# 服务器地址和端口
server_address = ('localhost', 33669)  # 修改为实际的服务器地址和端口

# 创建TCP客户端套接字
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    # 连接到服务器
    client_socket.connect(server_address)
    print("成功连接到服务器")

    # 接收和打印服务器发送的消息
    while True:
        data = client_socket.recv(1024)
        if data:
            print("收到消息：", data.decode())

except ConnectionRefusedError:
    print("无法连接到服务器")

except KeyboardInterrupt:
    print("程序已终止")

finally:
    # 关闭与服务器的连接
    client_socket.close()